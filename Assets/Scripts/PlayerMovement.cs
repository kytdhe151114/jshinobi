﻿using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerMovement : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private float moveSpeed = 5f;
        private Transform _transform;
        private Rigidbody2D _rigidbody2D;
        private BoxCollider2D _boxCollider2D;
        private Animator _animator;
        [SerializeField] private Transform throwPoint;
        [SerializeField] private GameObject shurikenPrefab;
        protected Vector2 movement;
        private float hDir;
        private float vDir;
        public float throwForce = 20f;
        void Start()
        {
            _transform = GetComponent<Transform>();
            _animator = GetComponent<Animator>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _boxCollider2D = GetComponent<BoxCollider2D>();
            _animator.SetFloat("Horizontal", 0);
            _animator.SetFloat("Vertical", -1);
        }

        // Update is called once per frame
        void Update()
        {
            Movement();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Attack();
            }
        }
    

        void FixedUpdate()
        {
        
        }

        private void Attack()
        {
            _animator.SetTrigger("Attack");
        }

        private void Movement()
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
            if (movement.x != 0 || movement.y != 0)
            {
                _rigidbody2D.MovePosition(_rigidbody2D.position + movement * moveSpeed * Time.fixedDeltaTime);
                _animator.SetFloat("Horizontal", movement.x);
                _animator.SetFloat("Vertical", movement.y);
                _animator.SetFloat("Speed", movement.SqrMagnitude());
            
                //Set position throwPoint
                throwPoint.position = new Vector3(
                    _transform.position.x + movement.x * 0.5f,
                    _transform.position.y + movement.y * 0.5f,
                    0);

                //Set rotation throwPoint
                float angle = Mathf.Atan2(movement.y, movement.x) * Mathf.Rad2Deg - 90f;
                Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                throwPoint.rotation = Quaternion.Slerp(throwPoint.rotation, rotation, 1);
            }
            else
            {
                _animator.SetFloat("Speed", 0);
            }
        
        }
        private void Throw()
        {
        

            //Initial Shuriken
            GameObject shuriken = Instantiate(shurikenPrefab, throwPoint.position, throwPoint.rotation);
            Rigidbody2D _rigidbody2D = shuriken.GetComponent<Rigidbody2D>();
            _rigidbody2D.AddForce(throwPoint.up * throwForce, ForceMode2D.Impulse);
        }

    }
}
