using UnityEngine;

namespace Assets.Scripts
{
    public class FieldOfView : MonoBehaviour
    {
        private Mesh mesh;
        private float fov;
        private Vector3 origin;
        private float startingAngle;
        public static Vector3 GetVectorFromAngle(float angle)
        {
            float angleRad = angle * (Mathf.PI / 180f);
            return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
        }

        void Start()
        {
            mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;
            fov = 90f;
            origin = Vector3.zero;
        }

        void Update() {
        
            int rayCount = 100;
            float angle = 30f;
            float angleIncrease = fov / rayCount;
            float viewDis = 1f;
        
        
        
            Vector3[] vertices = new Vector3[rayCount + 2];
            Vector2[] uv = new Vector2[vertices.Length];
            int[] triangles = new int[rayCount * 3];

            vertices[0] = origin;
            int vertexIndex = 1;
            int triangleIndex = 0;
            for (int i = 0; i <= rayCount; i++)
            {
                Vector3 vertex;
                RaycastHit2D raycasthit2D = Physics2D.Raycast(origin, GetVectorFromAngle(angle), viewDis);
                if (raycasthit2D.collider == null)
                {
                    vertex = origin + GetVectorFromAngle(angle) * viewDis;
                }
                else
                {
                    vertex = raycasthit2D.point;
                }
                vertices[vertexIndex] = vertex;
                if (i > 0)
                {
                    triangles[triangleIndex] = 0;
                    triangles[triangleIndex + 1] = vertexIndex - 1;
                    triangles[triangleIndex + 2] = vertexIndex;

                    triangleIndex += 3;
                }

                angle -= angleIncrease;
            

            }

            mesh.vertices = vertices;
            mesh.uv = uv;
            mesh.triangles = triangles;
        }

    }
}