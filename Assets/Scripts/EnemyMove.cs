using System.Collections.Generic;
using UnityEngine; //using NUnit.Framework.Interfaces;

namespace Assets.Scripts
{
    public class EnemyMove : MonoBehaviour
    {
        [SerializeField] private List<Vector3> location;
        [SerializeField] private List<float> waitTime;
        private int waitTimeIndex = 0;
        private int locationIndex = 0;
        private Vector3 point;
        public Transform player;
        public float moveSpeed = 5f;
        private Rigidbody2D rb;
        [SerializeField] private Transform pfFieldOfView;
        [SerializeField] private float viewDistance = 1f;
        [SerializeField] private float fov = 90f;
        private FieldOfView fieldOfView;
    
        private enum State {
            Waiting,
            Moving,
            AttackingPlayer,
            Busy,
        }
        private void Start()
        {
            rb = this.GetComponent<Rigidbody2D>();
            state = State.Waiting;
            fieldOfView = Instantiate(pfFieldOfView, null).GetComponent<FieldOfView>();
            // fieldOfView.SetFoV(fov);                                                   
            // fieldOfView.SetViewDistance(viewDistance);                                 
        }

        private State state;
        private float waitTimer;
        private Vector3 lastMoveDir;
        private void Update(){
            switch (state)
            {
                default:
                case State.Waiting:
                case State.Moving:
                    HandleMovement();
                    FindTargetPlayer();
                    break;
            }
        
            // fieldOfView.SetOrigin(transform.position);
            // fieldOfView.SetAimDirection(GetAimDir());
        }

        public Vector3 GetPosition() {
            return transform.position;
        }
        public Vector3 GetAimDir() {
            return lastMoveDir;
        }

        private void FindTargetPlayer() {
            if (Vector3.Distance(GetPosition(), player.transform.position ) < viewDistance) {
                // Player inside viewDistance
                Vector3 dirToPlayer = (player.transform.position - GetPosition()).normalized;
                if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f) {
                    // Player inside Field of View
                    RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                    if (raycastHit2D.collider != null) {
                        // Hit something
                        if (raycastHit2D.collider.gameObject.GetComponent<PlayerMovement>() != null) {
                            // Hit Player
                        } else {
                            // Do as usual
                        }
                    }
                }
            }
        }
    
        private void HandleMovement() {
            switch (state) {
                case State.Waiting:
                    waitTimer -= Time.deltaTime;
                    if (waitTimer <= 0f) {
                        state = State.Moving;
                    }
                    break;
                case State.Moving:
                    Vector3 waypoint = location[locationIndex];

                    Vector3 waypointDir = (waypoint - transform.position).normalized;
                    lastMoveDir = waypointDir;

                    float distanceBefore = Vector3.Distance(transform.position, waypoint);
                    transform.position = transform.position + waypointDir * moveSpeed * Time.deltaTime;
                    float distanceAfter = Vector3.Distance(transform.position, waypoint);

                    float arriveDistance = .1f;
                    if (distanceAfter < arriveDistance || distanceBefore <= distanceAfter) {
                        // Go to next waypoint
                        waitTimer = waitTime[locationIndex];
                        locationIndex = (locationIndex + 1) % location.Count;
                        state = State.Waiting;
                    }
                    break;
            }
        }
    
    }
}
                 
